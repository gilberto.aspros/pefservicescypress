# **QA Challenge**

## **1. Introduction**
The test automation framework was create in order to automate web applications.

Download the project and follow the steps in the next chapters, the results generated by this framework are:

1. Logs on execution time (console)
2. HTML report
3. Screenshots on error
4. Execution video

Requirements you need in your computer:
- Admin rights on your computer to download and install apps.
- Chocolatey package manager (only windows and optional)
- Homebrew (MAC and optional)
- Node => 14.15.1
- npm => 6.14.18
- Visual Studio Code, or other JS IDE.
- Supported browsers as of now: Chrome, Edge 93 and Firefox.

## **2. Tools**
the tools and programming language used on this project are:

1. JavaScript as Programming Language
2. Cypress for testing tool
3. Cucumber as BDD Framework
4. Chai for assertions
5. Git as control version software

## **3. Technology Stack**

### 3.1 Cypress
Cypress IO is a user-friendly test automation tool for end to end testing, UI testing, regression suites, integration and unit testing. It’s simple to install and doesn’t require any changes to your code. Cypress prides itself on writing tests quickly and in real-time as you build your web application. Often compared to Selenium, it differs by running tests within the device or browser being tested making it much faster.

### 3.2 JavaScript
JavaScript is a text-based programming language used both on the client-side and server-side that allows you to make web pages interactive. Where HTML and CSS are languages that give structure and style to web pages, JavaScript gives web pages interactive elements that engage a user. Common examples of JavaScript that you might use every day include the search box on Amazon, a news recap video embedded on The New York Times, or refreshing your Twitter feed.

### 3.3 Chai
Chai gives us the ability to easily write assertions. Chai gives us readable assertions with excellent error messages. Cypress extends this, fixes several common pitfalls, and wraps Chai's DSL using subjects and the ```.should()``` command. 

### 3.4 Cucumber
Cucumber is a tool that supports Behavior Driven Development (BDD). It offers a way to write tests that anybody can understand, regardless of their technical knowledge.

In BDD, users (business analysts, product owners) first write scenarios or acceptance tests that describes the behavior of the system from the customer's perspective, for review and sign-off by the product owners before developers write their codes.

### 3.5 Gherkin
Gherkin is a simple, lightweight and structured language which uses regular spoken language to describe requirements and scenarios. By regular spoken language we mean English, French and around 30 more languages.

As Gherkin is a structured language it follows some syntax let us first see a simple scenario described in gherkin.

```gherkin
Feature: Search feature for users
This feature is very important because it will allow users to filter products

Scenario: When a user searches, without spelling mistake, for a product name present in inventory. All the products with similar name should be displayed

Given User is on the main page of www.myshopingsite.com
When User searches for laptops
Then search page should be updated with the lists of laptops
```

### 3.6 Mochawesome
marge (mochawesome-report-generator) is the counterpart to mochawesome, a custom reporter for use with the Javascript testing framework, mocha. Marge takes the JSON output from mochawesome and generates a full fledged HTML/CSS report that helps visualize your test suites.

## **4. Steps to Execute**

### 4.1 Installing node and Git
If you have installed node 14.15 or above and git, skip this section

**NodeJS installation**
Navigate to https://nodejs.org/en/download/ and ownload the Node.js source code or a pre-built installer for your platform.

**Windows**
**Chocolatey installation**

Visit https://chocolatey.org/install and follow the installation steps.

**Git Installation**
1. Open a command line as administrator.
2. Copy and paste: ```choco install git -y```
3. Wait for the command to complete. If you don't see any errors, you are ready to use Git

**MAC**
**Homebrew installation**
Visit https://docs.brew.sh/Installation and follow the installation steps.

**Install Git**
Execute the following command on terminal
```
brew update
brew install git
```

### 4.2 Project Setup

1. To get started, first clone the repo from https://gitlab.com/gilberto.aspros/pefservicescypress, you can use the comand ```git clone https://gitlab.com/gilberto.aspros/pefservicescypress```
2. Open you IDE  
3. Open a terminal o command line
5. Execute ```npm install```
6. You can see a folder called *node_modules*

### 4.2 Project Setup

1. To get started, first clone the repo from https://gitlab.com/gilberto.aspros/pefservicescypress, you can use the comand ```git clone https://gitlab.com/gilberto.aspros/pefservicescypress.git```
2. Open you IDE  
3. Open a terminal o command line
5. Execute ```npm install```
6. You can see a folder called *node_modules*

### 4.3 Running the Project

**Running on Local**

You can run the Test Cases and generate the HTM L Report using the commands below
```npm run cy:run```
```npm run generate-report```

**Running in Cypress Dashboard**

Run cypress while passing the record key
Run the command below to start recording your tests!

```npm run cypress:dashboard```

## **5. Folder Sructure**

Proyect root is called *cypress*.

**Top - Level Directory Layout**

    .
    ├── downloads                        
    ├── fixtures                            
    ├── integration                       
    |   ├── common
    |   ├── test
    |       ├── api
    |       ├── ui                       
    |           ├── features
    |           ├── pages
    ├── plugins
    ├── reports
    ├── screenshots
    ├── support
    ├── videos
    ├── node_modules
    ├── .gitignore                                         
    ├── cypress.json                     
    ├── package-lock.json                
    ├── package.json                     
    └── README.md

## **7. How to ...?**

### Cucumber Expressions
We use https://docs.cucumber.io/cucumber/cucumber-expressions/ to parse your .feature file, please use that document as your reference


**Given/When/Then functions**
Since Given/When/Then are on global scope please use

```/* global Given, When, Then */```
to make IDE/linter happy or import them directly as shown in the above examples.

**Data table parameters**
To create steps that use gherkin data tables, the step definition needs to take an object and handle it like in these examples: Example Feature, Example Step Definition.

### Hooks

Hooks are used for setup and teardown the environment before and after each scenario. See the [API reference](./api_reference.md) for the specification of the first argument passed to hooks. Multiple *Before* hooks are executed in the order that they were defined. Multiple *After* hooks are executed in the **reverse** order that they were defined.

```javascript
var {After, Before} = require('@cucumber/cucumber');

// Synchronous
Before(function () {
  this.count = 0;
});

// Asynchronous Callback
Before(function (testCase, callback) {
  var world = this;
  tmp.dir({unsafeCleanup: true}, function(error, dir) {
    if (error) {
      callback(error);
    } else {
      world.tmpDir = dir;
      callback();
    }
  });
});

// Asynchronous Promise
After(function () {
  // Assuming this.driver is a selenium webdriver
  return this.driver.quit();
});
```

### Tagged hooks

Hooks can be conditionally selected for execution based on the tags of the scenario.

```javascript
var {After, Before} = require('@cucumber/cucumber');

Before(function () {
  // This hook will be executed before all scenarios
});

Before({tags: "@foo"}, function () {
  // This hook will be executed before scenarios tagged with @foo
});

Before({tags: "@foo and @bar"}, function () {
  // This hook will be executed before scenarios tagged with @foo and @bar
});

Before({tags: "@foo or @bar"}, function () {
  // This hook will be executed before scenarios tagged with @foo or @bar
});

// You can use the following shorthand when only specifying tags
Before("@foo", function () {
  // This hook will be executed before scenarios tagged with @foo
});
```

See more documentation on [tag expressions](https://docs.cucumber.io/cucumber/api/#tag-expressions)

### Skipping in a Before Hook

If you need to imperatively skip a test using a `Before` hook, this can be done using any of the constructs defined in [skipped steps](./step_definitions.md)

This includes using: a synchronous return, an asynchronous callback, or an asynchronous promise

```javascript
// Synchronous
Before(function() {
  // perform some runtime check to decide whether to skip the proceeding scenario
  return 'skipped'
});
```

### BeforeAll / AfterAll

If you have some setup / teardown that needs to be done before or after all scenarios, use `BeforeAll` / `AfterAll`. Like hooks and steps, these can be synchronous, accept a callback, or return a promise.

Unlike `Before` / `After` these methods will not have a world instance as `this`. This is because each scenario gets its own world instance and these hooks run before / after **all** scenarios.

```javascript
var {AfterAll, BeforeAll} = require('@cucumber/cucumber');

// Synchronous
BeforeAll(function () {
  // perform some shared setup
});

// Asynchronous Callback
BeforeAll(function (callback) {
  // perform some shared setup

  // execute the callback (optionally passing an error when done)
});

// Asynchronous Promise
AfterAll(function () {
  // perform some shared teardown
  return Promise.resolve()
});
```

### BeforeStep / AfterStep

If you have some code execution that needs to be done before or after all steps, use `BeforeStep` / `AfterStep`. Like the `Before` / `After` hooks, these also have a world instance as 'this', and can be conditionally selected for execution based on the tags of the scenario.

```javascript
var {AfterStep, BeforeStep} = require('@cucumber/cucumber');

BeforeStep({tags: "@foo"}, function () {
  // This hook will be executed before all steps in a scenario with tag @foo
});

AfterStep( function ({result}) {
  // This hook will be executed after all steps, and take a screenshot on step failure
  if (result.status === Status.FAILED) {
    this.driver.takeScreenshot();
  }
});
```

## Background section
Adding a background section to your feature will enable you to run steps before every scenario. For example, we have a counter that needs to be reset before each scenario. We can create a given step for resetting the counter.

```gherkin
let counter = 0;

Given("counter has been reset", () => {
  counter = 0;
});

When("counter is incremented", () => {
  counter += 1;
});

Then("counter equals {int}", value => {
  expect(counter).to.equal(value);
});
```
```gherkin
Feature: Background Section
  
   Background:
    Given counter has been reset

   Scenario: Basic example #1
     When counter is incremented
     Then counter equals 1

   Scenario: Basic example #2
     When counter is incremented
     When counter is incremented
     Then counter equals 2
```

**Sharing context**
You can share context between step definitions using cy.as() alias.

Example:

```gherkin
Given('I go to the add new item page', () => {
  cy.visit('/addItem');
});

When('I add a new item', () => {
  cy.get('input[name="addNewItem"]').as('addNewItemInput');
  cy.get('@addNewItemInput').type('My item');
  cy.get('button[name="submitItem"]').click();
})

Then('I see new item added', () => {
  cy.get('td:contains("My item")');
});

Then('I can add another item', () => {
  expect(cy.get('@addNewItemInput').should('be.empty');
});

```

## Smart tagging
Start your tests without setting any tags. And then put a @focus on the scenario (or scenarios) you want to focus on while development or bug fixing.

For example:

```gherkin
Feature: Smart Tagging

  As a cucumber cypress plugin which handles Tags
  I want to allow people to select tests to run if focused
  So they can work more efficiently and have a shorter feedback loop

  Scenario: This scenario should not run if @focus is on another scenario
    Then this unfocused scenario should not run

  @focus
  Scenario: This scenario is focused and should run
    Then this focused scenario should run

  @this-tag-affects-nothing
  Scenario: This scenario should also not run
    Then this unfocused scenario should not run

  @focus
  Scenario: This scenario is also focused and also should run
    Then this focused scenario should run
```

### How to run the tests*
Run your Cypress Launcher the way you would usually do, for example:

```./node_modules/.bin/cypress open```
click on a ```.feature``` file on the list of specs, and see the magic happening!

### Running tagged tests
You can use tags to select which test should run using cucumber's tag expressions. Keep in mind we are using newer syntax, eg. ```'not @foo and (@bar or @zap)'```. In order to initialize tests using tags you will have to run cypress and pass TAGS environment variable.

Example:

```./node_modules/.bin/cypress-tags run -e TAGS='not @foo and (@bar or @zap)'```

Please note - we use our own cypress-tags wrapper to speed things up. This wrapper calls the cypress executable from local modules and if not found it falls back to the globally installed one. For more details and examples please take a look to the example repo.

## **8. Results**

### Dashboard

![Dashboard](./images/dashboard.png?raw=true "Dashboard")

### Logs
![Logs](./images/logs.png?raw=true "Logs")

### HTML
We
![HTML01](./images/htmlreport01.png?raw=true "HTML01")
![HTML02](./images/htmlreport02.png?raw=true "HTML02")
![HTML03](./images/htmlreport03.png?raw=true "HTML03")
