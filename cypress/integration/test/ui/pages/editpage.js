/// <reference types="cypress" />

export class EditPage{
    
    typeComputerName(computerName){
        cy.typeOnElement('#name', computerName);
    }

    typeIntroducedDate(date){
        cy.typeOnElement('#introduced',date);
    }

    typeDiscontinued(date){
        cy.typeOnElement('#discontinued',date);
    }

    selectCompany(item){
        cy.selectFromMenu(item);
    }

    computerInfo(computerName, IntroducedDate, discontinuedDate, company){
        this.typeComputerName(computerName);
        this.typeIntroducedDate(IntroducedDate);
        this.typeDiscontinued(discontinuedDate)
        this.selectCompany(company);
    }

    saveChanges(){
        cy.clickOnElementByXpath(".//*[@value='Save this computer']");
    }

    clickDeleteComputer(){
        cy.clickOnElementByXpath(".//*[@value='Delete this computer']");
    }

    cancelChanges(){
        cy.clickByText('Cancel')
    }

    getErrorMessage(error){
        cy.getElementText(error);
    }

    editComputer(){
        this.saveChanges();
    }

    deleteComputer(){
        this.clickDeleteComputer();
        cy.getElementText('has been deleted');
    }

    verifyComputerUpdated(){
        cy.getElementText('has been updated');
    }

    invalidIntroduced(date, error){
        this.typeIntroducedDate(date);
        this.saveChanges();
        this.getErrorMessage(error);
    }

    invalidDiscontinued(date, error){
        this.typeDiscontinued(date);
        this.saveChanges();
        this.getErrorMessage(error);
    }
    
}

export const editPage = new EditPage();