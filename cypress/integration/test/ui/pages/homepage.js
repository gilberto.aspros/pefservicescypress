/// <reference types="cypress" />

export class HomePage{
    loadHomePage() {
        cy.visit('/', { 
          onBeforeLoad: (win) => { win.sessionStorage.clear() }
         });
      }

    searchComputer(computer){
        cy.typeOnElement("#searchbox",computer);
        cy.clickOnElement("#searchsubmit");
        cy.getElementText(computer);
    }

    editComputer(computer){
        cy.clickByText(computer);
    }

    addNewComputer(){
        cy.clickOnElement("#add");
    }

    getAllComputers(){
        cy.getElementsText('.//tr');
    }

    goToFinalDisplayingPage(){
        for(var i=0;i<3;i++){
            cy.get('#pagination > ul > li.next').click({force:true});
        }
    }
}

export const homePage = new HomePage();