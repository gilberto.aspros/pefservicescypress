/// <reference types="cypress" />

import { EditPage } from "./editpage";

export class CreateNewPage extends EditPage{
   
    createComputer(computerName, IntroducedDate, discontinuedDate, company){
        this.computerInfo(computerName, IntroducedDate, discontinuedDate, company);
        this.clickOnCreateComputer();
    }

    clickOnCreateComputer(){
        cy.clickOnElementByXpath(".//*[@value='Create this computer']");
    }
}

export const createNewPage = new CreateNewPage();