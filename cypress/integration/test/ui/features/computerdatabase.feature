Feature: QA automation test code challenge

    Scenario: Failure validation displays after modify Introduced date at Commodore64
        Given I search computer "Commodore 64"
        When I edit the computer "Commodore 64"
        Then I should see an error after type invalid "introduced" date
        |Introduced|Error                |
        |randomtext|Failed to decode date|

    Scenario: Failure validation displays after modify Discontinued date at Commodore64
        Given I search computer "Commodore 64"
        When I edit the computer "Commodore 64"
        Then I should see an error after type invalid "discontinued" date
        |Discontinued|Error                                        |
        |randomtext  |Failed to decode date                        |
        |1000-01-01  |Discontinued date is before introduction date|

    Scenario: Valid data is updated succesfully after modify Commodore64
        Given I search computer "Commodore 64"
        When I edit the computer "Commodore 64"
        And modify computer data
        |Computer_Name    |Introduced_Date|Discontinued_Date|Company|
        |Commodore 64     |1989-08-01     |2000-01-01       |ASUS   |
        Then I should see my computer modified

    Scenario: Retrive all HP computer list
        Given I search computer "HP"
        Then I should see all computer items

    Scenario: Retrive last list of IBM computers
        Given I search computer "IBM"
        When I navigate to last results page
        Then I should see all computer items

    Scenario: Create a new computer 
        Given I add a new computer
        When I type computer info
        |Computer_Name    |Introduced_Date|Discontinued_Date|Company|
        |Gilberto Computer|1982-08-01     |1994-01-01       |Nokia  |
        Then I should see my computer created