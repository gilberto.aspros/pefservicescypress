/// <reference types="Cypress"/>
/// <reference types="cypress-xpath" />

import {Given, And, Then} from "cypress-cucumber-preprocessor/steps"
import {homePage} from "../../pages/homepage"
import {editPage} from "../../pages/editpage"
import {createNewPage} from "../../pages/createnewpage"

Given(`I search computer {string}`, (computer) =>{
    homePage.searchComputer(computer);
})

When(`I edit the computer {string}`, (computer) =>{
    homePage.editComputer(computer);
})

Then(`I should see an error after type invalid {string} date`, (date, datevalue) =>{

    datevalue.hashes().forEach(row =>{
        if(date === "introduced"){
            editPage.invalidIntroduced(row.Introduced, row.Error);
        }else{
            editPage.invalidDiscontinued(row.Discontinued, row.Error);
        }
    });
    
    editPage.cancelChanges();
})

Given(`I add a new computer`, () =>{
    homePage.addNewComputer();
})

When(`modify computer data`, computerInfo =>{
    computerInfo.hashes().forEach(row =>{
        editPage.computerInfo(row.Computer_Name,row.Introduced_Date,row.Discontinued_Date,row.Company);
    });
    editPage.saveChanges();
})

Then(`I should see all computer items`, () =>{
    homePage.getAllComputers();
})

Then(`I should see my computer modified`, () =>{
    editPage.verifyComputerUpdated();
})

When(`I navigate to last results page`, () =>{
    homePage.goToFinalDisplayingPage();
})

Given(`I add a new computer`, () =>{
    homePage.addNewComputer();
})

When(`I type computer info`, computerInfo =>{
    computerInfo.hashes().forEach(row =>{
        createNewPage.createComputer(row.Computer_Name,row.Introduced_Date,row.Discontinued_Date,row.Company);
    });
})

Then(`I should see my computer created`, () =>{
    cy.getElementText('Gilberto Computer has been created');
})




