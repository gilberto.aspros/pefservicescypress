// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add("selectFromMenu", (item) => {
    cy.get('select')
    .should('be.visible')
    .select(item);
})

Cypress.Commands.add("getElementsText", (element) => {
    cy.xpath(element,{
        timeout: 20000})
        .should('be.visible')
        .each(($subElement) => {
            if(!$subElement.text().includes('name')){
                cy.log($subElement.text())
            }
    });
})

Cypress.Commands.add("clickOnElement", (element) => {
    cy.get(element)
    .should('be.visible')
    .click({force:true});    
})

Cypress.Commands.add("clickFirstElementByText", (element) => {
    cy.xpath(`.//*[contains(text(),'${element}')]`,{timeout: 20000})
        .should('be.visible')
        .first().click()
});

Cypress.Commands.add("clickByText", (element, index=1) => {
    cy.xpath(`(.//*[contains(text(),'${element}')])[${index}]`,{timeout: 30000})
    .should('be.visible')
    .click({force:true});    
})

Cypress.Commands.add("clickOnElementByXpath", (element, index=1) => { 
    cy.xpath(`(${element})[${index}]`,{timeout: 30000})
    .should('be.visible')
    .click({force:true});    
})

Cypress.Commands.add("getElementText", (text, index=1) => {
    cy.xpath(`(.//*[contains(text(),'${text}')])[${index}]`,{timeout: 30000})
    .should('be.visible')
    .invoke('text')
    .then(($text) => {
        expect($text).contains(text)
    });
})

Cypress.Commands.add("containElementText", (element, text, index=1) => {
    cy.wait(2000);
    cy.log(text);
    cy.log(element);
    cy.xpath(`(${element})[${index}]`,{timeout: 30000})
    .should('be.visible')
    .invoke('text')
    .then(($text) => {
        cy.log($text);
        expect(text).equal($text)
    });
})

Cypress.Commands.add("typeOnElementByXpath", (element, value) => {
    cy.xpath(element,{timeout: 30000})
    .should('be.visible')
    .clear()
    cy.xpath(element,{timeout: 30000}).type(value)
})

Cypress.Commands.add("typeOnElement", (element, value) => {
    cy.get(element,{timeout: 30000})
    .should('be.visible')
    .clear()
    .type(value)
})
