const report = require('multiple-cucumber-html-reporter');

report.generate({
	jsonDir: 'cypress/cucumber-json',
	reportPath: './reports/',
	metadata:{
        browser: {
            name: 'chrome',
            version: '90'
        },
        device: 'Local test machine',
        platform: {
            name: 'windows',
            version: '10'
        }
    },
    customData: {
        title: 'Run info',
        data: [
            {label: 'Project', value: 'Medallion project'},
            {label: 'Release', value: '1.2.3'},
        ]
    }
});


/* var reporter = require('multiple-cucumber-html-reporter');
var options = {

        theme: 'bootstrap',

        jsonFile: 'cypress/cucumber-json',

        output: './reports/cucumber-htmlreport.html',

        reportSuiteAsScenarios: true,

        scenarioTimestamp: true,

        launchReport: true,

        metadata: {

            "App Version":"0.3.2",

            "Test Environment": "STAGING",

            "Browser": "Chrome",

            "Platform": "Windows 10",

            "Parallel": "Scenarios",

            "Executed": "Remote"

        }

    };

    reporter.generate(options); */